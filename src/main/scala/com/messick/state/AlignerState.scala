package com.messick.state

/**
  * Created by jason on 10/1/16.
  */
class AlignerState {

  type AlignerAction[A] = A => A
  type Align[A, L] = ((A, L)) => (A, L)
  type StrAlign = Align[String, List[String]]


  val A1: AlignerAction[String] = (s) => s + "1"
  val A2: AlignerAction[String] = (s) => s + "2"

  val R1: StrAlign = (tuple) => (A1(tuple._1), log(tuple._1) :: tuple._2)
  val R2: StrAlign = (tuple) => (A2(tuple._1), log(tuple._1) :: tuple._2)

  def log(s: String) = s"aligned $s"

  def run() = {
    val composed: (String, List[String]) => (String, List[String]) = (s, l) => R2(R1((s, l)))
    println(composed)
    println(composed("test", List()))

    val alignments: List[StrAlign] = List(R1, R2)
    println(alignments)

    val result = alignments.foldLeft(("test", List[String]()))((t, f) => f(t))
    println(result)

    def alignString(s: String): (String, List[String]) = alignments.foldLeft(s, List[String]())((t, f) => f(t))
    val stringList = List("StringA", "StringB", "StringC")
    println(stringList.map(alignString))
  }

}

object AlignerState extends App {
  val as = new AlignerState
  as.run()

}
