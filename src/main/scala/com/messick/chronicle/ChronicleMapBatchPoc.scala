package com.messick.chronicle

import java.io.File
import java.lang.{Integer => JInt, Long => JLong}

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.{Done, NotUsed}
import net.openhft.chronicle.core.values.{IntValue, LongValue}
import net.openhft.chronicle.map.{ChronicleMap, ChronicleMapBuilder}
import net.openhft.chronicle.values.Values

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.Random

/**
  * Created by jason on 9/1/17.
  */
object ChronicleMapBatchPoc extends App {

  implicit val system = ActorSystem("akka-stream-sample")

  // takes the list of transformations and materializes them in the form of org.reactivestreams.Processor instances
  implicit val materializer = ActorMaterializer()

  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  val fileName1K = "test-1K.dat"
  val fileName10M = "test-10M.dat"
  val fileName250M = "test-250M.dat"

  val entries1K = 1000
  val entries10M = 10000000
  val entries250M = 250000000

  timedCacheCreate(fileName1K, entries1K)
  timedCacheCreate(fileName10M, entries10M)
  timedCacheCreate(fileName250M, entries250M)
  system.terminate()

  def timedCacheCreate(fileName: String, entryCount: Int): Unit = {
    val batchCount = 100
    val batchSize = (entryCount / batchCount) + 1


    val start = System.currentTimeMillis()

    val numbers: Source[Int, NotUsed] = Source.fromIterator(() => Iterator.range(0, entryCount))


    var count = 1
    val futurePutRandom:(Seq[Int] => Future[Seq[LongValue]]) = i => {

      val file = new File(fileName + count.toString)
      count = count + 1
      val cache: ChronicleMap[IntValue, LongValue] = ChronicleMapBuilder
        .of(classOf[IntValue], classOf[LongValue])
        .entries(i.size)
        .maxBloatFactor(2.5)
        .putReturnsNull(true)
        .createPersistedTo(file)
      val fs = i.map { n =>
        Future {
          val key = Values.newHeapInstance(classOf[IntValue])
          key.setValue(Random.nextInt)
          val value = Values.newHeapInstance(classOf[LongValue])
          value.setValue(50000L)
          cache.put(key, value)
        }
      }
      Future.sequence(fs)

    }
    val future: Future[Done] = numbers.grouped(batchSize).mapAsyncUnordered(2)(futurePutRandom).runWith(Sink.ignore)
    Await.ready(future, Duration.Inf)

    val fullFile = new File(s"FullCache$entryCount.db")
    val fullCache: ChronicleMap[IntValue, LongValue] = ChronicleMapBuilder
      .of(classOf[IntValue], classOf[LongValue])
      .entries(entryCount)
      .maxBloatFactor(2.5)
      .putReturnsNull(true)
      .createPersistedTo(fullFile)

    for (i <- 1 to batchCount) {
      fullCache.putAll(new File(fileName + i))
    }

    println(s"Created $entryCount entries in ${System.currentTimeMillis() - start} ms")
  }


}
