package com.messick.chronicle

import java.io.File

import net.openhft.chronicle.bytes.ref.{BinaryIntReference, BinaryLongReference}
import net.openhft.chronicle.core.values.{IntValue, LongValue}
import net.openhft.chronicle.map.{ChronicleMap, ChronicleMapBuilder}
import java.lang.{Integer => JInt, Long => JLong}

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import net.openhft.chronicle.values.Values

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.Random

/**
  * Created by jason on 9/1/17.
  */
object ChronicleMapPoc extends App {

  implicit val system = ActorSystem("akka-stream-sample")

  // takes the list of transformations and materializes them in the form of org.reactivestreams.Processor instances
  implicit val materializer = ActorMaterializer()

  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  val fileName1K = "./test-1K.dat"
  val fileName10M = "./test-10M.dat"
  val fileName250M = "./test-250M.dat"

  val entries1K = 1000
  val entries10M = 10000000
  val entries250M = 250000000

  timedCacheCreate(fileName1K, entries1K)
  timedCacheCreate(fileName10M, entries10M)
  timedCacheCreate(fileName250M, entries250M)
  system.terminate()

  def timedCacheCreate(fileName: String, entryCount: Int): Unit = {
    val file = new File(fileName)
    val cache: ChronicleMap[IntValue, LongValue] = ChronicleMapBuilder
      .of(classOf[IntValue], classOf[LongValue])
      .entries(entryCount)
      .maxBloatFactor(2.5)
      .allowSegmentTiering(true)
      .putReturnsNull(true)
      .createPersistedTo(file)

    val start = System.currentTimeMillis()

    val numbers: Source[Int, NotUsed] = Source.fromIterator(() => Iterator.range(0, entryCount))
    val futurePutRandom: (Int => Future[Unit]) = i => {
      Future {
        val key = Values.newHeapInstance(classOf[IntValue])
        key.setValue(Random.nextInt)
        val value = Values.newHeapInstance(classOf[LongValue])
        value.setValue(50000L)
        cache.put(key, value)
      }
    }
    val future: Future[Done] = numbers.mapAsyncUnordered(8)(futurePutRandom).runWith(Sink.ignore)
    Await.ready(future, Duration.Inf)

    println(s"Created $entryCount entries in ${System.currentTimeMillis() - start} ms")
  }


}
