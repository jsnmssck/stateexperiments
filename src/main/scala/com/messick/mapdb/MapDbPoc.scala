package com.messick.mapdb

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import org.mapdb.{DB, DBMaker, Serializer}

import scala.collection.mutable.ListBuffer
import scala.util.Random
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.collection.JavaConverters._
/**
  * Created by jason on 9/2/17.
  */
object MapDbPoc extends App {


  implicit val system = ActorSystem("akka-stream-sample")

  // takes the list of transformations and materializes them in the form of org.reactivestreams.Processor instances
  implicit val materializer = ActorMaterializer()

  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  val fileName1K = "./mapdb-1K.dat"
  val fileName10M = "./mapdb-10M.dat"
  val fileName250M = "./mapdb-250M.dat"
  val entries1K = 1000
  val entries10M = 10000000
  val entries250M = 250000000

  timedCacheCreate(fileName1K, entries1K)
  timedCacheCreate(fileName10M, entries10M)
  timedCacheCreate(fileName250M, entries250M)
  system.terminate()

  def timedCacheCreate(fileName: String, entryCount: Int): Unit = {

    val db: DB = DBMaker
      .fileDB(fileName)
      .allocateStartSize(entryCount)
      .fileMmapEnable()
      .fileMmapPreclearDisable()
      .concurrencyScale(20)
//        .executorEnable()
//      .allocateStartSize( 10 * 1024*1024*1024)  // 10GB
//      .allocateIncrement(512 * 1024*1024)       // 512MB
      .make()

    val start = System.currentTimeMillis()
    val cache = db
      .treeMap("map", Serializer.INTEGER, Serializer.LONG)
//      .counterEnable()
      .createOrOpen()
    val numbers: Source[Int, NotUsed] = Source.fromIterator(() => Iterator.range(0, entryCount))
    val futurePutRandom: (Seq[Int]) => Future[Unit] = i => Future(cache.putAll(i.map(n => (Integer.valueOf(Random.nextInt()), java.lang.Long.valueOf(50000))).toMap.asJava))
    val future: Future[Done] = numbers.grouped(100000).mapAsyncUnordered(100)(futurePutRandom).runWith(Sink.ignore)
    Await.ready(future, Duration.Inf)
    db.commit()

    println(s"Created $entryCount entries in ${System.currentTimeMillis() - start} ms")
    println(s"Size: ${cache.size}")
    db.close()

  }


}
