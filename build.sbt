name := "StateExperiments"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "net.openhft" % "chronicle-map" % "3.14.1"

// https://mvnrepository.com/artifact/org.mapdb/mapdb
libraryDependencies += "org.mapdb" % "mapdb" % "3.0.5"
// https://mvnrepository.com/artifact/com.typesafe.akka/akka-stream_2.11
libraryDependencies += "com.typesafe.akka" % "akka-stream_2.11" % "2.5.4"
